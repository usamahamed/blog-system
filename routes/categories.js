var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
var db=require('monk')('localhost/nodeblog');

router.get('/show/:category', function(req, res, next) {
	var db=req.db;
	var posts=db.get('posts');
	posts.find({category:req.params.category},{},function(err,posts){
		res.render('index',{
  		"title":req.params.title,
  		"posts":posts
  	});
	});

});
router.get('/add', function(req, res, next) {
res.render('addcategory',{
  		"title":"addcategory"
  	});
});

router.post('/add',function(req,res,next){

	//get form values
	var title=req.body.title;

//form validation
req.checkBody('title',"title is require").notEmpty();
//check error
var errors = req.validationErrors();
if(errors) {
	res.render('/addcategory',{
		"errors":errors,
		"title":title
	})
}else{
	var categories=db.get('categories');
	//submit to db
	categories.insert({
		"title":title
	},function(err,categories){
		if (err) {
			res.send("there is an issue submitting category");
		}else{
			req.flash('sucess',"category submmited");
			res.location('/');
			res.redirect('/');
		}
	});

}
});
module.exports = router;
