var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
var flash=require('connect-flash');

var db=require('monk')('localhost/nodeblog');
router.get('/add',function(req,res,next){
	var categories=db.get('categories');
	categories.find({},{},function(err,categories){
		res.render('addpost',{
			"title":"Add post",
			"categories":categories
	});

});
});
router.get('/show/:id',function(req,res,next){
	var posts=db.get('posts');
	posts.findById(req.params.id,function(err,post){
		res.render('show',{
			
			"post":post
	});
	});
});

router.post('/add',function(req,res,next){

	//get form values
	var title=req.body.title;
var category=req.body.category;
var body=req.body.body;
var author=req.body.Author;
var date = new Date();

if(req.file.mainimage){
var mainImageOrignalName    = req.file.mainimage.originalname;
var mainImageName 		   	= req.file.mainimage.name;
var mainImagemime           =req.file.mainimage.mimetype;
var mainImagepath           =req.file.mainimage.path;
var mainImageExt            =req.file.mainimage.extension;
var mainImagesize           =req.file.mainimage.size;
}else{
var mainImageName = "noimage.png";
}
//form validation
req.checkBody('title',"title is require").notEmpty();
req.checkBody('body',"body is require");
//check error
var errors = req.validationErrors();
if(errors) {
	res.render('/addpost',{
		"errors":errors,
		"title":title,
		"body":body
	})
}else{
	var posts=db.get('posts');
	//submit to db
	posts.insert({
		"title":title,
		"body":body,
		"category":category,
		"date":date,
		"author":author,
		"mainimage":mainImageName
	},function(err,posts){
		if (err) {
			res.send("there is an issue submitting post");
		}else{
			req.flash('sucess',"post submmited");
			res.location('/');
			res.redirect('/');
		}
	});

}
});

router.post('/addcomment',function(req,res,next){

	//get form values
	var name=req.body.name;
var email=req.body.email;
var body=req.body.body;
var postid=req.body.postid;
var commentdate = new Date();


//form validation
req.checkBody('name',"name is require").notEmpty();
req.checkBody('email',"email is require").notEmpty();
req.checkBody('email',"name is not formattef").isEmail();
req.checkBody('body',"body is require").notEmpty();
//check error
var errors = req.validationErrors();
if(errors) {
	var posts=db.get('posts');
	posts.findById(postid,function(err,post){
res.render('/show',{
		"errors":errors,
		"post":title
	});
	});
	
}else{
	var comment={"name":name,"email":email,"body":body,"commentdate":commentdate};
		var posts=db.get('posts');

	//submit to db
	posts.update({
		"_id":postid}
		,
		{$push:{
			"comments":comment
		}},function(err,doc){
			if(err) {
				throw err;
			}else{
				req.flash('sucess','comments added');
				res.location('/post/show/'+postid);
				res.redirect('/post/show/'+postid);

			}
		}
		);

}
});
module.exports = router;
