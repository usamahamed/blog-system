var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var expressValidator = require('express-Validator');
var bodyParser = require('body-parser');
var mongo=require('mongodb');
var db=require('monk')('localhost/nodeblog');
var multer=require('multer');
var flash=require('connect-flash');
var index = require('./routes/index');
var post = require('./routes/post');
var categories = require('./routes/categories');
var app = express();

app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});
//handle express session
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  resave:true
}));
//connect flash
  app.use(flash());
app.locals.moment = require('moment');

//
app.locals.truncateText= function(text,length){
var truncateText= text.substring(0,length);
return truncateText;
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


//handle file upload
app.use(multer({dest:'./public/images/uploads'}).single('mainimage'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//handle express session
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  resave:true
}));

app.use(function(req, res, next) {
  // open connection
  req.db = db;
  next();
});




//handle validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));


app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

app.use('/', index);
app.use('/post', post);
app.use('/categories', categories);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
